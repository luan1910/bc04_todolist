import logo from "./logo.svg";
import "./App.css";
import TodoList from "./BaiTapToDoList/TodoList/TodoList";

function App() {
  return <TodoList />;
}

export default App;
