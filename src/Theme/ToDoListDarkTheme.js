export const ToDoListDarkTheme = {
  bgColor: "#343a40",
  color: "#fff",
  boderButton: "1px solid #fff",
  borderRadiusButton: "none",
  hoverTextColor: "#343a40",
  hoverBgColor: "#fff",
  borberColor: "#343a40",
};
